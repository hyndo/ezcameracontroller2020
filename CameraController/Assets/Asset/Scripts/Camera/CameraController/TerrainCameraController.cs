﻿using UnityEngine;

/// <summary>
/// Camera controller that takes some of its constraints from a Unity Terrain
/// </summary>
public class TerrainCameraController : AConstrainedCameraController
{
    protected override float MinimalZoom { get => _MinimalZoom; set => _MinimalZoom = value; }
    protected override float MaximalZoom { get => _MaximalZoom; set => _MaximalZoom = value; }
    protected override float XAxisRotationMin { get => _XAxisRotationMin; set => _XAxisRotationMin = value; }
    protected override float XAxisRotationMax { get => _XAxisRotationMax; set => _XAxisRotationMax = value; }
    protected override Vector2 VerticalConstraints { get => _VerticalConstraints; set => _VerticalConstraints = value; }
    protected override Vector4 HorizontalConstraints { get => _HorizontalConstraints; set => _HorizontalConstraints = value; }

    [Header("Constraints")]
    [Range(0f, 0.5f)]
    [SerializeField] private float _MinimalZoom = 0.1f;
    [Range(0.5f, 1f)]
    [SerializeField] private float _MaximalZoom = 0.9f;
    [Range(0f, 0.5f)]
    [SerializeField] private float _MinimalHeight = 0.1f;
    [Range(0.5f, 1f)]
    [SerializeField] private float _MaximalHeight = 0.9f;
    [Range(CameraControllerConstants.MINIMAL_X_AXIS_ROTATION_NEGATIVE_RAW, CameraControllerConstants.MINIMAL_X_AXIS_ROTATION)]
    [SerializeField] private float _XAxisRotationMin = -45;
    [Range(CameraControllerConstants.MINIMAL_X_AXIS_ROTATION, CameraControllerConstants.MAXIMAL_X_AXIS_ROTATION)]
    [SerializeField] private float _XAxisRotationMax = 45;
    [Header("Terrain")]
    [SerializeField] private Terrain _Terrain = null;
    
    private Vector2 _VerticalConstraints = Vector2.one;
    private Vector4 _HorizontalConstraints = Vector4.one;

    protected override void InitConstraints()
    {
        Vector3 terrainPosition = _Terrain.transform.position;
        Vector3 terrainSize = _Terrain.terrainData.size;

        _VerticalConstraints = new Vector2
        (
            terrainSize.y * _MinimalHeight + terrainPosition.y, 
            terrainSize.y * _MaximalHeight + terrainPosition.y
        );

        _HorizontalConstraints = new Vector4
        (
            terrainPosition.x, 
            terrainPosition.x + terrainSize.x, 
            terrainPosition.z, 
            terrainPosition.z + terrainSize.z
        );

        base.InitConstraints();
    }
}
