﻿using UnityEngine;
using Constants = CameraControllerConstants;

/// <summary>
/// Used for calculating shift in horizontal plane constraints, based on rotation, zoom and height of the camera
/// </summary>
public class CameraShiftController : MonoBehaviour
{
    /// <summary>
    /// Returns shift for the horizontal constraints
    /// </summary>
    /// <param name="camera"></param>
    /// <param name="trackingCameraTransform"></param>
    /// <param name="horizontalConstraints"></param>
    /// <returns></returns>
    public Vector3 GetShift(Camera camera, Transform trackingCameraTransform,Vector2 verticalConstraints, Vector4 horizontalConstraints)
    {
        float heightAboveGround = camera.transform.position.y;
        Vector3 eulerAngles = camera.transform.eulerAngles;
        float angleOfCamera = 0;
        
        if (eulerAngles.x >= Constants.MINIMAL_X_AXIS_ROTATION && eulerAngles.x <= Constants.MAXIMAL_X_AXIS_ROTATION)
        {
            angleOfCamera = Mathf.Clamp(Constants.QUARTER_ARC - camera.transform.eulerAngles.x - (camera.fieldOfView / 2) * Constants.FOV_ANGLE_POSITIVE_MULTIPLIER, 0, Constants.QUARTER_ARC);
        }
           
        if(eulerAngles.x < Constants.MAXIMAL_X_AXIS_ROTATION_NEGATIVE && eulerAngles.x >= Constants.MINIMAL_X_AXIS_ROTATION_NEGATIVE)
        {
            angleOfCamera = Mathf.Clamp(Constants.QUARTER_ARC - (360-camera.transform.eulerAngles.x) - (camera.fieldOfView / 2) * Constants.FOV_ANGLE_POSITIVE_MULTIPLIER, 0, Constants.QUARTER_ARC);
            float verticalRange = Mathf.Abs(verticalConstraints.x - verticalConstraints.y);
            heightAboveGround = (verticalRange - heightAboveGround)*1.5f;
        }
           
        float remainingAngle = Constants.QUARTER_ARC - angleOfCamera; 

        float shift = heightAboveGround * Mathf.Sin(Mathf.Deg2Rad * angleOfCamera)/Mathf.Sin(Mathf.Deg2Rad * remainingAngle);

        Vector3 direction = GetDirection(camera.transform, trackingCameraTransform);
        shift = Mathf.Clamp(shift, 0, GetShiftConstraint(direction, horizontalConstraints));
        return direction * shift;
    }

    private Vector3 GetDirection(Transform cameraTransform, Transform trackingCameraTransform)
    {
        Vector3 direction = (trackingCameraTransform.position - cameraTransform.position).normalized;
        direction.z *= -1;
        return direction;
    }

    private float GetShiftConstraint(Vector3 direction, Vector4 horizontalConstraint)
    {
        float phase = Mathf.Abs(direction.x);
        Vector4 newConstraints = CameraControllerConstants.HORIZONTAL_CONSTRAINTS_LIMIT_MULTIPLIER * horizontalConstraint;
        float limitX = Mathf.Abs(newConstraints.x - newConstraints.y);
        float limitZ = Mathf.Abs(newConstraints.z - newConstraints.w);
        return Mathf.Lerp(limitX,limitZ,phase);
    }
}
