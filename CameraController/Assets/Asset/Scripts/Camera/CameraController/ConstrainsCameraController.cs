﻿using UnityEngine;

/// <summary>
/// Camera controller that uses direct values as constraints 
/// </summary>
public class ConstrainsCameraController : AConstrainedCameraController
{
    protected override float MinimalZoom { get => _MinimalZoom; set => _MinimalZoom = value; }
    protected override float MaximalZoom { get => _MaximalZoom; set => _MaximalZoom = value; }
    protected override float XAxisRotationMin { get => _XAxisRotationMin; set => _XAxisRotationMin = value; }
    protected override float XAxisRotationMax { get => _XAxisRotationMax; set => _XAxisRotationMax = value; }
    protected override Vector2 VerticalConstraints { get => _VerticalConstraints; set => _VerticalConstraints = value; }
    protected override Vector4 HorizontalConstraints { get => _HorizontalConstraints; set => _HorizontalConstraints = value; }

    [Header("Constraints")]
    [Range(0f, 0.5f)]
    [SerializeField] private float _MinimalZoom = 0.1f;
    [Range(0.5f, 1f)]
    [SerializeField] private float _MaximalZoom = 0.9f;
    [Range(CameraControllerConstants.MINIMAL_X_AXIS_ROTATION_NEGATIVE_RAW, CameraControllerConstants.MINIMAL_X_AXIS_ROTATION)]
    [SerializeField] private float _XAxisRotationMin = 0;
    [Range(CameraControllerConstants.MINIMAL_X_AXIS_ROTATION, CameraControllerConstants.MAXIMAL_X_AXIS_ROTATION)]
    [SerializeField] private float _XAxisRotationMax = 90;
    [SerializeField] private Vector2 _VerticalConstraints = Vector2.one;
    [SerializeField] private Vector4 _HorizontalConstraints = Vector4.one;
}
