﻿using UnityEngine;
using System.Collections;
using System;

/// <summary>
/// Base camera controller, from which all other camera controllers inherit
/// </summary>
[Serializable]
public abstract class ACameraController : MonoBehaviour
{
    /// <summary>
    /// Indicates whether a special action (special key + mouse scrollwheel) can be used
    /// </summary>
    public bool                                                                 SpecialAction { get; private set;}

    /// <summary>
    /// Indicates whether the camera is allowed to move
    /// </summary>
    public bool                                                                 AllowMovement { get => _AllowMovement; set { _AllowMovement = value; }}

    /// <summary>
    /// Indicates whether the camera is allowed to rotate
    /// </summary>
    public bool                                                                 AllowRotation { get => _AllowRotation; set { _AllowRotation = value; }}

    /// <summary>
    /// Indicates whether the camera is allowed to scroll
    /// </summary>
    public bool                                                                 AllowScroll { get => _AllowScroll; set { _AllowScroll = value; }}

    /// <summary>
    /// Indicates whether the camera is allowed to zoom
    /// </summary>
    public bool                                                                 AllowZoom { get => _AllowZoom; set { _AllowZoom = value; }}
    
    [Header("Behaviour")]
    [SerializeField] private bool                                               _AllowMovement = true;
    [SerializeField] private bool                                               _AllowRotation = true;
    [SerializeField] private bool                                               _AllowScroll = false;
    [SerializeField] private bool                                               _AllowZoom = false;
    [SerializeField] private bool                                               _AllowMovementDampening = false;
    [SerializeField] private bool                                               _AllowRotationDampening = false;
    [SerializeField] private bool                                               _AllowScrollDampening = false;
    [SerializeField] private bool                                               _AllowZoomDampening = false;
    [Header("Behaviour settings")]
    [Range(1f, 500f)]
    [SerializeField] private float                                              _CameraMovementSpeed = 50f;
    [Range(1f, 500f)]
    [SerializeField] private float                                              _CameraRotationSpeed = 50f;
    [Range(1f, 500f)]
    [SerializeField] private float                                              _CameraScrollSpeed = 50f;
    [Range(1f, 500f)]
    [SerializeField] private float                                              _CameraZoomSpeed = 50f;
    [Range(1f, 1000f)]
    [SerializeField] private float                                              _MovementDampeningAmount = 250f;
    [Range(1f, 1000f)]
    [SerializeField] private float                                              _RotationDampeningAmount = 250f;
    [Range(1f, 1000f)]
    [SerializeField] private float                                              _ScrollDampeningAmount = 250f;
    [Range(1f, 1000f)]
    [SerializeField] private float                                              _ZoomDampeningAmount = 250f;
    [Header("Defaults")]
    [SerializeField] protected Vector3                                          _DefaultCameraPosition = new Vector3(50,50,10);
    [SerializeField] protected Vector3                                          _DefaultCameraRotation = Vector3.zero;
    [Header("References")]
    [SerializeField] protected Camera                                           _Camera = null;
    [SerializeField] private CameraControllerInputInterpreter                                   _InputInterpreter = null;
    [SerializeField] protected CameraShiftController                            _CameraShiftController = null;

    /// <summary>
    /// Transform of the actual camera. Meant for receiving changes in rotation.
    /// </summary>
    protected Transform                                                         _CameraTransform = null;

    /// <summary>
    /// Transform of the parent of the camera. Meant for receiving changes in position
    /// </summary>
    protected Transform                                                         _ParentCameraTransform = null;

    /// <summary>
    /// Transform meant to indicate a direction from the camera transform 
    /// </summary>
    protected Transform                                                         _TrackingCameraTransform = null;

    /// <summary>
    /// Constraints for camera rotation and movement
    /// </summary>
    protected Constraints                                                       _Constraints;

    private Array                                                               _CameraActionTypeVals = null;
    private bool                                                                _ScrollEngaged = false;
    private bool                                                                _ZoomEngaged = false;
    private float                                                               _AdjustedScrollDampeningAmount => _ScrollDampeningAmount/150;
    private float                                                               _AdjustedZoomDampeningAmount => _ZoomDampeningAmount/250;


    private SpeedController                                                     _SpeedController = null;
    private VectorController                                                    _VectorController = null;
    private CameraActionController                                              _CameraActionController = null;

    /* MONOBEHAVIOUR */
    private void Awake() 
    {
        // camera action types      
        _CameraActionTypeVals = Enum.GetValues(typeof(CameraActionTypes));

        // parent camera transform
        _CameraTransform = _Camera.transform;
        _ParentCameraTransform = new GameObject("CameraParent").GetComponent<Transform>();
        _ParentCameraTransform.rotation = new Quaternion();
        _ParentCameraTransform.eulerAngles = new Vector3(-45, 0, 0);
        _ParentCameraTransform.position = _CameraTransform.position;
        _CameraTransform.parent = _ParentCameraTransform;
        _TrackingCameraTransform = new GameObject("CameraTracking").GetComponent<Transform>();
        _TrackingCameraTransform.position = _ParentCameraTransform.position - _CameraTransform.forward;
        _TrackingCameraTransform.rotation = new Quaternion();
        _TrackingCameraTransform.parent = _ParentCameraTransform;

        // subscription
        _InputInterpreter.movementSet += OnMovementSet;
        _InputInterpreter.movementMuted += OnMovementMuted;
        _InputInterpreter.rotationSet += OnRotationSet;
        _InputInterpreter.rotationMuted += OnRotationMuted;
        _InputInterpreter.scrollSet += OnScrollSet;
        _InputInterpreter.scrollMuted += OnScrollMuted;
        _InputInterpreter.scrollSet += OnZoomSet;
        _InputInterpreter.scrollMuted += OnZoomMuted;
        _InputInterpreter.specialAction += OnSpecialAction; 
    }
    
    void Start()
    {
        SpeedControllerDto speedDto = new SpeedControllerDto()
        {
            cameraActionTypeVals = _CameraActionTypeVals,
            allowMovementDampening = _AllowMovementDampening,
            allowRotationDampening = _AllowRotationDampening,
            allowScrollDampening = _AllowScrollDampening,
            allowZoomDampening = _AllowZoomDampening  
        };
        _SpeedController = new SpeedController(speedDto);

        VectorControllerDto vectorDto = new VectorControllerDto()
        {
            cameraActionTypeVals = _CameraActionTypeVals
        };
        _VectorController = new VectorController(vectorDto);

        CameraActionDto actionDto = new CameraActionDto()
        {
            SpeedController = _SpeedController,
            VectorController = _VectorController,
            CameraActionTypeValues = _CameraActionTypeVals
        };
        _CameraActionController = new CameraActionController(actionDto);

        InitConstraints();
        InitDefaults();
    }
    
    void Update()
    {
        for(int i=0; i< _CameraActionTypeVals.Length; i++)
        {
            CameraActionTypes type = (CameraActionTypes)i;
            _CameraActionController.ActionDelegates[type]?.Invoke(_VectorController.LastActiveVector[type]);
        }
        
        ConstrainCamera();
    }

    /* MOVEMENT */
    private void OnMovementSet(MovementVectors vector)
    {
        if(AllowMovement) _CameraActionController.ExecuteCameraAction(CameraActionTypes.MOVEMENT, vector, MoveCamera);
    }
    
    private void OnMovementMuted(MovementVectors vector)
    {
        if(AllowMovement)_CameraActionController.MuteCameraActionGeneric(CameraActionTypes.MOVEMENT, vector);
    }

    private void MoveCamera(MovementVectors vector)
    {
        _SpeedController.CurrentAxisSpeed[CameraActionTypes.MOVEMENT] = Mathf.Clamp01(_SpeedController.CurrentAxisSpeed[CameraActionTypes.MOVEMENT] + 1/_MovementDampeningAmount);
        float speed = _SpeedController.SpeedDelegates[CameraActionTypes.MOVEMENT].Invoke(_CameraMovementSpeed);
        switch(vector)
        {
            case MovementVectors.FWD:
            _ParentCameraTransform.Translate(_TrackingCameraTransform.forward * speed, Space.World);
            break;
            case MovementVectors.BWD:
            _ParentCameraTransform.Translate(-_TrackingCameraTransform.forward * speed, Space.World);
            break;
            case MovementVectors.LFT:
            _ParentCameraTransform.Translate(-_TrackingCameraTransform.right * speed, Space.World);
            break;
            case MovementVectors.RGHT:
            _ParentCameraTransform.Translate(_TrackingCameraTransform.right * speed, Space.World);
            break;
            default: throw new NotImplementedException();
        }
    }

    /* ROTATION */
    private void OnRotationSet(MovementVectors vector)
    {
        if(AllowRotation)_CameraActionController.ExecuteCameraAction(CameraActionTypes.ROTATION, vector, RotateCamera);
    }

    private void OnRotationMuted(MovementVectors vector)
    {
        if(AllowRotation)_CameraActionController.MuteCameraActionGeneric(CameraActionTypes.ROTATION, vector);   
    }

    private void RotateCamera(MovementVectors vector)
    {
        _SpeedController.CurrentAxisSpeed[CameraActionTypes.ROTATION] = Mathf.Clamp01(_SpeedController.CurrentAxisSpeed[CameraActionTypes.ROTATION] + 1/_RotationDampeningAmount);
        float speed = _SpeedController.SpeedDelegates[CameraActionTypes.ROTATION].Invoke(_CameraRotationSpeed);
        switch(vector)
        {
            case MovementVectors.LFT:
            _ParentCameraTransform.Rotate(new Vector3(0,-speed,0), Space.World);
            break;
            case MovementVectors.RGHT:
            _ParentCameraTransform.Rotate(new Vector3(0,speed,0), Space.World);
            break;
            default: throw new NotImplementedException();
        }
    }
    
    /* SCROLL */
    private void OnScrollSet(MovementVectors vector)
    {
        if(_SpeedController.CurrentAxisSpeed[CameraActionTypes.ZOOM] == 0f && !SpecialAction && AllowScroll)
        {
            StopAllCoroutines();
            _ScrollEngaged = true;
            _CameraActionController.ExecuteCameraAction(CameraActionTypes.SCROLL, vector, ScrollCamera);
        }    
    }

    private void OnScrollMuted(MovementVectors vector)
    {   
        if(_SpeedController.CurrentAxisSpeed[CameraActionTypes.ZOOM] == 0f && !SpecialAction && AllowScroll)
        {
            StartCoroutine(MuteScrollCor(vector));
        }
    }

    private IEnumerator MuteScrollCor(MovementVectors vector)
    {
        if(AllowScroll && !SpecialAction)
        {
            _ScrollEngaged = false;
            while(_SpeedController.CurrentAxisSpeed[CameraActionTypes.SCROLL] > 0)
            {
                if(_AllowScrollDampening)
                    _SpeedController.CurrentAxisSpeed[CameraActionTypes.SCROLL] = Mathf.Clamp01(_SpeedController.CurrentAxisSpeed[CameraActionTypes.SCROLL] - 1/(_AdjustedScrollDampeningAmount*100));
                else 
                {
                    float speed = _SpeedController.SpeedDelegates[CameraActionTypes.SCROLL].Invoke(_CameraScrollSpeed);
                    _SpeedController.CurrentAxisSpeed[CameraActionTypes.SCROLL] = Mathf.Clamp01(_SpeedController.CurrentAxisSpeed[CameraActionTypes.SCROLL] - speed/4);
                }
                    
                yield return null;
            }
            _CameraActionController.MuteCameraActionScrollable(vector, CameraActionTypes.SCROLL);
            _ScrollEngaged = true;
        }
    }

    private void ScrollCamera(MovementVectors vector)
    {

        if(_ScrollEngaged) _SpeedController.CurrentAxisSpeed[CameraActionTypes.SCROLL] = Mathf.Clamp01(_SpeedController.CurrentAxisSpeed[CameraActionTypes.SCROLL] + 1/_AdjustedScrollDampeningAmount);
        float speed = _SpeedController.SpeedDelegates[CameraActionTypes.SCROLL].Invoke(_CameraScrollSpeed);
        switch(vector)
        {
            case MovementVectors.UP:
            _ParentCameraTransform.Translate(_ParentCameraTransform.up * speed, Space.World);
            break;
            case MovementVectors.DOWN:
            _ParentCameraTransform.Translate(-_ParentCameraTransform.up * speed, Space.World);
            break;
            default: throw new NotImplementedException();
        }
        
    }

    /* ZOOM */
    private void OnZoomSet(MovementVectors vector)
    {
        if(_SpeedController.CurrentAxisSpeed[CameraActionTypes.SCROLL] == 0f && SpecialAction && AllowZoom)
        {
            StopAllCoroutines();
            _ZoomEngaged = true;
            _CameraActionController.ExecuteCameraAction(CameraActionTypes.ZOOM, vector, ZoomCamera);
        }
    }

    private void OnZoomMuted(MovementVectors vector)
    {
        if(_SpeedController.CurrentAxisSpeed[CameraActionTypes.SCROLL] == 0f && SpecialAction && AllowZoom)
        {
            StartCoroutine(MuteZoomCor(vector));
        }
    }

    private IEnumerator MuteZoomCor(MovementVectors vector)
    {
        if(AllowZoom && SpecialAction)
        {
            _ZoomEngaged = false;
            while(_SpeedController.CurrentAxisSpeed[CameraActionTypes.ZOOM] > 0)
            {
                if(_AllowZoomDampening)
                    _SpeedController.CurrentAxisSpeed[CameraActionTypes.ZOOM] = Mathf.Clamp01(_SpeedController.CurrentAxisSpeed[CameraActionTypes.ZOOM] - 1/(_AdjustedZoomDampeningAmount*100));
                else
                {
                    float speed = _SpeedController.SpeedDelegates[CameraActionTypes.ZOOM].Invoke(_CameraZoomSpeed);
                    _SpeedController.CurrentAxisSpeed[CameraActionTypes.ZOOM] = Mathf.Clamp01(_SpeedController.CurrentAxisSpeed[CameraActionTypes.ZOOM] - speed/4);
                }
                    
                yield return null;
            }
            _CameraActionController.MuteCameraActionScrollable(vector, CameraActionTypes.ZOOM);
            _ZoomEngaged = true;
        }
    }

    private void ZoomCamera(MovementVectors vector)
    {

        if(_ZoomEngaged) _SpeedController.CurrentAxisSpeed[CameraActionTypes.ZOOM] = Mathf.Clamp01(_SpeedController.CurrentAxisSpeed[CameraActionTypes.ZOOM] + 1/_AdjustedZoomDampeningAmount);
        float speed = _SpeedController.SpeedDelegates[CameraActionTypes.ZOOM].Invoke(_CameraZoomSpeed);

        if(_Camera.orthographic)
        {

        }
        else
        {
            switch(vector)
            {
                case MovementVectors.UP:
                _Camera.fieldOfView += speed;
                break;
                case MovementVectors.DOWN:
                _Camera.fieldOfView -= speed;
                break;
                default: throw new NotImplementedException();
            }
        }
    }

    private void OnSpecialAction(bool state)
    {
        SpecialAction = state;
    }

    /* CONSTRAINTS AND DEFAULTS */
    /// <summary>
    /// Initializes constraints for camera actions
    /// </summary>
    protected abstract void InitConstraints();
    /// <summary>
    /// Initializes default values for things like camera position, rotation etc. 
    /// </summary>
    protected abstract void InitDefaults();
    /// <summary>
    /// Executes the actual constraining of the camera according to the initialized constraints
    /// </summary>
    protected abstract void ConstrainCamera();
}