﻿using UnityEngine;

/// <summary>
/// A camera controller from which all other controllers with implemented constraints inherit 
/// </summary>
public abstract class AConstrainedCameraController : ACameraController
{
    /// <summary>
    /// Minimal zoom allowed for the camera
    /// </summary>
    protected abstract float                                    MinimalZoom {get; set;}

    /// <summary>
    /// Maximal zoom allowed for the camera
    /// </summary>
    protected abstract float                                    MaximalZoom {get;set;}

    /// <summary>
    /// Minimal rotation in the x-axis allowed for the camera
    /// </summary>
    protected abstract float                                    XAxisRotationMin {get;set;}

    /// <summary>
    /// Maximal rotation in the x-axis allowed for the camera
    /// </summary>
    protected abstract float                                    XAxisRotationMax {get;set;}

    /// <summary>
    /// Camera constraints in terms height above floor
    /// </summary>
    protected abstract Vector2                                  VerticalConstraints {get;set;}

    /// <summary>
    /// Camera constraints in the horizontal movement plane
    /// </summary>
    protected abstract Vector4                                  HorizontalConstraints {get;set;}

    private muteDelegate                                        _ConstrainZoomMethod = null;

    protected override void ConstrainCamera()
    {
        Vector3 shift = _CameraShiftController.GetShift(_Camera, _TrackingCameraTransform, VerticalConstraints, HorizontalConstraints);

        Vector3 cameraPosition = _ParentCameraTransform.position;
        Vector4 hc = _Constraints.HorizontalConstraint;
        Vector2 vc = _Constraints.VerticalConstraint;
        _ParentCameraTransform.position = new Vector3
        (
            Mathf.Clamp(cameraPosition.x, hc.x + shift.x, hc.y + shift.x), // left, right
            Mathf.Clamp(cameraPosition.y, vc.x, vc.y), // down, up
            Mathf.Clamp(cameraPosition.z, hc.z - shift.z, hc.w - shift.z) // backward, forward
        );

        /* rotation */
        Vector3 cameraRotation = _CameraTransform.eulerAngles;
        Vector2 xac = _Constraints.XAxisRotationConstraint;

        if(cameraRotation.x >= CameraControllerConstants.MINIMAL_X_AXIS_ROTATION && cameraRotation.x <= CameraControllerConstants.MAXIMAL_X_AXIS_ROTATION)// pozitivni cisla 0 - 90
        {
            _CameraTransform.eulerAngles = new Vector3
            (
                Mathf.Clamp(cameraRotation.x, xac.x, xac.y), // min, max
                cameraRotation.y,
                cameraRotation.z
            );
        }
        else if(cameraRotation.x > CameraControllerConstants.MAXIMAL_X_AXIS_ROTATION && Mathf.Round(cameraRotation.x) < CameraControllerConstants.MINIMAL_X_AXIS_ROTATION_NEGATIVE) // pozitivni cisla mezi maximalnim pozitivnim a hranici negativnich
        {
            _CameraTransform.eulerAngles = new Vector3
            (
                CameraControllerConstants.MAXIMAL_X_AXIS_ROTATION,
                cameraRotation.y,
                cameraRotation.z
            );
        }
        else if(cameraRotation.x < CameraControllerConstants.MAXIMAL_X_AXIS_ROTATION_NEGATIVE &&  cameraRotation.x >= CameraControllerConstants.MINIMAL_X_AXIS_ROTATION_NEGATIVE) // od hranice negativnich do 360
        {
            Vector2 translatedArc = new Vector2(TranslateNegativeArc(xac.x), TranslateNegativeArc(xac.y));
            _CameraTransform.eulerAngles = new Vector3
            (
                 Mathf.Clamp(cameraRotation.x, translatedArc.x, translatedArc.y), // min, max
                cameraRotation.y,
                cameraRotation.z
            );
        }

        _ConstrainZoomMethod?.Invoke();
    }

    protected override void InitConstraints()
    {
        _Constraints = new Constraints
        (
            new Vector2(MinimalZoom, MaximalZoom),
            VerticalConstraints,
            HorizontalConstraints,
            new Vector2(XAxisRotationMin, XAxisRotationMax)
        );

        if(_Camera.orthographic) _ConstrainZoomMethod = ConstrainOrthographicCamera;
        else _ConstrainZoomMethod = ConstrainPerspectiveCamera;

        if(!_Constraints.CheckValidity()) 
            Debug.LogError("User constraints are not valid!");
    }

    protected override void InitDefaults()
    {
        _ParentCameraTransform.position = _DefaultCameraPosition;

        float xRotation = _DefaultCameraRotation.x;
        if(xRotation >= 0)
        {
            _CameraTransform.eulerAngles = new Vector3
            (
                Mathf.Clamp(_DefaultCameraRotation.x, CameraControllerConstants.MINIMAL_X_AXIS_ROTATION, CameraControllerConstants.MAXIMAL_X_AXIS_ROTATION),
                _DefaultCameraRotation.y,
                _DefaultCameraRotation.z
            );
        }
        else if(xRotation < 0)
        {
            _CameraTransform.eulerAngles = new Vector3
            (
                Mathf.Clamp(TranslateNegativeArc(_DefaultCameraRotation.x), CameraControllerConstants.MINIMAL_X_AXIS_ROTATION_NEGATIVE, CameraControllerConstants.MAXIMAL_X_AXIS_ROTATION_NEGATIVE),
                _DefaultCameraRotation.y,
                _DefaultCameraRotation.z
            );
        }
    }

    /// <summary>
    /// Constrains perspective camera according to its fov
    /// </summary>
    protected void ConstrainPerspectiveCamera()
    {
        Vector2 fovc = _Constraints.FOVConstraint;
        float min = fovc.x * 180; float max = fovc.y * 180;
        _Camera.fieldOfView = Mathf.Clamp(_Camera.fieldOfView, min, max);
    }

    /// <summary>
    /// Constrains othographic camera according to its size
    /// </summary>
    protected void ConstrainOrthographicCamera()
    {
        Vector2 fovc = _Constraints.FOVConstraint;
        float min = fovc.y * 50; float max = fovc.x * 50;
        _Camera.orthographicSize = Mathf.Clamp(_Camera.orthographicSize, min, max);
    }

    private float TranslateNegativeArc(float negativeAngle)
    {
        return Mathf.Clamp(360 + negativeAngle, 0, 360);
    }
}
