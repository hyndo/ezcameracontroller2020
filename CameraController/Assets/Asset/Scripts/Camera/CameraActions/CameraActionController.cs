﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

/// <summary>
/// Controller for executing and stopping camera actions like moving, rotating, scrolling and zooming
/// </summary>
public class CameraActionController 
{
    /// <summary>
    /// Primary constructor
    /// </summary>
    /// <param name="dto"></param>
    public CameraActionController(CameraActionDto dto)
    {
        _SpeedController = dto.SpeedController;
        _VectorController = dto.VectorController;
        ActionDelegates = new Dictionary<CameraActionTypes, movementVectorDelegate>();
        for(int i=0; i< dto.CameraActionTypeValues.Length; i++)
        {
            CameraActionTypes type = (CameraActionTypes)i;
            ActionDelegates.Add(type,null);
        }
    }

    private SpeedController                                                    _SpeedController;
    private VectorController                                                   _VectorController;

    /// <summary>
    /// Dictionary of associations between camera action types and methods that perform them
    /// </summary>
    public Dictionary<CameraActionTypes, movementVectorDelegate>               ActionDelegates {get; set; }

    /// <summary>
    /// Adds a camera action for execution in update loop
    /// </summary>
    /// <param name="actionType"> Type of action that should be added for execution </param>
    /// <param name="vector"> The direction of the action </param>
    /// <param name="method"> Method that should execute the action </param>
    public void ExecuteCameraAction(CameraActionTypes actionType, MovementVectors vector, movementVectorDelegate method)
    {
        _SpeedController.CurrentAxisSpeed[actionType] = 0f;
        if(!_VectorController.ActiveVectors[actionType].Contains(vector))
            _VectorController.ActiveVectors[actionType].Add(vector);
        _VectorController.LastActiveVector[actionType] = vector;
        if(ActionDelegates[actionType] == null) ActionDelegates[actionType] = method;
    }

    /// <summary>
    /// Removes generic (non-scrolling) action from execution in update loop
    /// </summary>
    /// <param name="actionType"></param>
    /// <param name="vector"></param>
    public void MuteCameraActionGeneric(CameraActionTypes actionType, MovementVectors vector)
    {
        if(actionType != CameraActionTypes.SCROLL && actionType != CameraActionTypes.ZOOM)
        {
            _VectorController.ActiveVectors[actionType].Remove(vector);
            if (_VectorController.ActiveVectors[actionType].Count > 0)
            {
                _VectorController.LastActiveVector[actionType] = _VectorController.ActiveVectors[actionType].Last();
            }
            else
            {
                _VectorController.LastActiveVector[actionType] = MovementVectors.NONE;
                _SpeedController.CurrentAxisSpeed[actionType] = 0f;
                ActionDelegates[actionType] = null;
            }
        }
        else
        {
            Debug.LogWarning("Tried to mute nonscrollable action as scrollable. Doing nothing.");
        }
    }

    /// <summary>
    /// Removes scrollable action from execution in update loop
    /// </summary>
    /// <param name="vector"></param>
    /// <param name="actionType"></param>
    public void MuteCameraActionScrollable(MovementVectors vector, CameraActionTypes actionType)
    {
        if (actionType == CameraActionTypes.SCROLL || actionType == CameraActionTypes.ZOOM)
        {
            ActionDelegates[actionType] = null;
            _VectorController.ActiveVectors[actionType].Remove(MovementVectors.UP);
            _VectorController.ActiveVectors[actionType].Remove(MovementVectors.DOWN);
            _VectorController.ActiveVectors[actionType].Remove(vector);
            if (_VectorController.ActiveVectors[actionType].Count > 0) _VectorController.LastActiveVector[actionType] = _VectorController.ActiveVectors[actionType].Last();
            else _VectorController.LastActiveVector[actionType] = MovementVectors.NONE;
        }
        else
        {
            Debug.LogWarning("Tried to mute nonscrollable action as scrollable. Doing nothing.");
        }
    }
}
