﻿using System;

/// <summary>
/// Data transfer object for CameraActionController initialization
/// </summary>
public struct CameraActionDto
{
    /// <summary>
    /// Values of the CameraActionTypes enumerator
    /// </summary>
    public Array CameraActionTypeValues {get; set;}

    /// <summary>
    /// A reference to a SpeedController
    /// </summary>
    public SpeedController SpeedController {get; set;}

    /// <summary>
    /// A reference to a VectorController
    /// </summary>
    public VectorController VectorController {get; set;}
}
