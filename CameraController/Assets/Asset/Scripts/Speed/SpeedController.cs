﻿using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Handles the speed at which all the camera actions are performing
/// </summary>
public class SpeedController
{
    /// <summary>
    /// Main constructor
    /// </summary>
    /// <param name="dto"></param>
    public SpeedController(SpeedControllerDto dto)
    {
        CurrentAxisSpeed = new Dictionary<CameraActionTypes, float>();
        SpeedDelegates = new Dictionary<CameraActionTypes, returnDelegate<float>>();
        for(int i=0; i< dto.cameraActionTypeVals.Length; i++)
        {
            CameraActionTypes type = (CameraActionTypes)i;
            CurrentAxisSpeed.Add(type, 0f);
            SpeedDelegates.Add(type,null);
        }

        if(dto.allowMovementDampening) SpeedDelegates[CameraActionTypes.MOVEMENT] = MovementSpeedWithDampening;
        else SpeedDelegates[CameraActionTypes.MOVEMENT] = MovementSpeed;
        if(dto.allowRotationDampening) SpeedDelegates[CameraActionTypes.ROTATION] = RotationSpeedWithDampening;
        else SpeedDelegates[CameraActionTypes.ROTATION] = RotationSpeed;
        if(dto.allowScrollDampening) SpeedDelegates[CameraActionTypes.SCROLL] = ScrollSpeedWithDampening;
        else SpeedDelegates[CameraActionTypes.SCROLL] = ScrollSpeed;
        if(dto.allowZoomDampening) SpeedDelegates[CameraActionTypes.ZOOM] = ZoomSpeedWithDampening;
        else SpeedDelegates[CameraActionTypes.ZOOM] = ZoomSpeed;
    }

    /// <summary>
    /// Contains current speed at which each of the camera actions is performed 
    /// </summary>
    public Dictionary<CameraActionTypes, float>                                CurrentAxisSpeed { get; set; }

    /// <summary>
    /// Stores the method for calculating speed for every camera action
    /// </summary>
    public Dictionary<CameraActionTypes, returnDelegate<float>>                SpeedDelegates { get; set; }

    private float RotationSpeed(float cameraRotationSpeed){return Time.deltaTime * cameraRotationSpeed;}

    private float MovementSpeed(float cameraMovementSpeed){return Time.deltaTime * cameraMovementSpeed;}

    private float ScrollSpeed(float cameraScrollSpeed){return Time.deltaTime * cameraScrollSpeed;}

    private float ZoomSpeed(float cameraZoomSpeed){return Time.deltaTime * cameraZoomSpeed;}

    private float RotationSpeedWithDampening(float cameraRotationSpeed){ return RotationSpeed(cameraRotationSpeed) * CurrentAxisSpeed[CameraActionTypes.ROTATION]; }
    
    private float MovementSpeedWithDampening(float cameraMovementSpeed){ return MovementSpeed(cameraMovementSpeed) * CurrentAxisSpeed[CameraActionTypes.MOVEMENT]; }

    private float ScrollSpeedWithDampening(float cameraScrollSpeed){ return ScrollSpeed(cameraScrollSpeed) * CurrentAxisSpeed[CameraActionTypes.SCROLL]; }

    private float ZoomSpeedWithDampening(float cameraZoomSpeed){ return ZoomSpeed(cameraZoomSpeed) * CurrentAxisSpeed[CameraActionTypes.ZOOM]; }
}