﻿using System;

/// <summary>
/// Data transfer object for initializing SpeedController
/// </summary>
public struct SpeedControllerDto
{
    /// <summary>
    /// Values of the CameraActionType enumerator
    /// </summary>
    public Array cameraActionTypeVals {get; set; }

    /// <summary>
    /// Indicates whether movement dampening is allowed
    /// </summary>
    public bool allowMovementDampening {get; set; }

    /// <summary>
    /// Indicates whether rotation dampening is allowed
    /// </summary>
    public bool allowRotationDampening {get; set; }

    /// <summary>
    /// Indicates whether scroll dampening is allowed
    /// </summary>
    public bool allowScrollDampening {get; set; }

    /// <summary>
    /// Indicates whether zoom dampening is allowed
    /// </summary>
    public bool allowZoomDampening {get; set; }
}
