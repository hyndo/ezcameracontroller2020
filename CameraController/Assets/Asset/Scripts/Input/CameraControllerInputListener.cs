﻿using UnityEngine;
using UnityEngine.InputSystem;

/// <summary>
/// Listens to hardware input and passes it on for interpretation
/// </summary>
public class CameraControllerInputListener : MonoBehaviour
{
    /// <summary>
    /// Fires when movement forward occurs
    /// </summary>
    public event muteDelegate movementForwardListened;

    /// <summary>
    /// Fires when movement backward occurs
    /// </summary>
    public event muteDelegate movementBackwardListened;

    /// <summary>
    /// Fires when movement left occurs
    /// </summary>
    public event muteDelegate movementLeftListened;

    /// <summary>
    /// Fires when movement right occurs
    /// </summary>
    public event muteDelegate movementRightListened;

    /// <summary>
    /// Fires when movement forward stops
    /// </summary>
    public event muteDelegate movementForwardMuted;

    /// <summary>
    /// Fires when movement backward stops
    /// </summary>
    public event muteDelegate movementBackwardMuted;

    /// <summary>
    /// Fires when movement left stops
    /// </summary>
    public event muteDelegate movementLeftMuted;

    /// <summary>
    /// Fires when movement right stops
    /// </summary>
    public event muteDelegate movementRightMuted;

    /// <summary>
    /// Fires when rotation left occurs
    /// </summary>
    public event muteDelegate rotateLeftListened;

    /// <summary>
    /// Fires when rotation right occurs
    /// </summary>
    public event muteDelegate rotateRightListened;

    /// <summary>
    /// Fires when rotation left stops
    /// </summary>
    public event muteDelegate rotateLeftMuted;

    /// <summary>
    /// Fires when rotation right stops
    /// </summary>
    public event muteDelegate rotateRightMuted;

    /// <summary>
    /// Fires when mouse scroll occurs
    /// </summary>
    public event valueDelegate<float> scrollListened;

    /// <summary>
    /// Fires when mouse scroll stops
    /// </summary>
    public event valueDelegate<float> scrollMuted;

    /// <summary>
    /// Fires to indicate whether sepecial action occurs
    /// </summary>
    public event valueDelegate<bool> specialAction;

    private CameraControllerInput _Input = null;

    void Awake()
    {
        _Input = new CameraControllerInput();

        _Input.StandardInput.MovementForward.started += OnMovementForwardListened;
        _Input.StandardInput.MovementBackward.started += OnMovementBackwardListened;
        _Input.StandardInput.MovementLeft.started += OnMovementLeftListened;
        _Input.StandardInput.MovementRight.started += OnMovementRightListened;
        _Input.StandardInput.MovementForward.canceled += OnMovementForwardMuted;
        _Input.StandardInput.MovementBackward.canceled += OnMovementBackwardMuted;
        _Input.StandardInput.MovementLeft.canceled += OnMovementLeftMuted;
        _Input.StandardInput.MovementRight.canceled += OnMovementRightMuted;

        _Input.StandardInput.RotateLeft.started += OnRotateLeftListened;
        _Input.StandardInput.RotateRight.started += OnRotateRightListened;
        _Input.StandardInput.RotateLeft.canceled += OnRotateLeftMuted;
        _Input.StandardInput.RotateRight.canceled += OnRotateRightMuted;

        _Input.StandardInput.Scroll.started += OnScrollListened;
        _Input.StandardInput.Scroll.canceled += OnScrollMuted;

        _Input.StandardInput.SpecialAction.started += OnSpecialActionListened;
        _Input.StandardInput.SpecialAction.canceled += OnSpecialActionMuted;
    }

    void OnEnable()
    {
        _Input.Enable();
    }

    private void OnDisable() 
    {
        _Input.Disable();
    }

    private void OnMovementForwardListened(InputAction.CallbackContext context)
    {
        movementForwardListened?.Invoke();
    }

    private void OnMovementForwardMuted(InputAction.CallbackContext context)
    {
        movementForwardMuted?.Invoke();
    }

    private void OnMovementBackwardListened(InputAction.CallbackContext context)
    {
        movementBackwardListened?.Invoke();
    }

    private void OnMovementBackwardMuted(InputAction.CallbackContext context)
    {
        movementBackwardMuted?.Invoke();
    }

    private void OnMovementLeftListened(InputAction.CallbackContext context)
    {
        movementLeftListened?.Invoke();
    }

    private void OnMovementLeftMuted(InputAction.CallbackContext context)
    {
        movementLeftMuted?.Invoke();
    }

    private void OnMovementRightListened(InputAction.CallbackContext context)
    {
        movementRightListened?.Invoke();
    }

    private void OnMovementRightMuted(InputAction.CallbackContext context)
    {
        movementRightMuted?.Invoke();
    }

    private void OnRotateLeftListened(InputAction.CallbackContext context)
    {
        rotateLeftListened?.Invoke();
    }

    private void OnRotateLeftMuted(InputAction.CallbackContext context)
    {
        rotateLeftMuted?.Invoke();
    }

    private void OnRotateRightListened(InputAction.CallbackContext context)
    {
        rotateRightListened?.Invoke();
    }

    private void OnRotateRightMuted(InputAction.CallbackContext context)
    {
        rotateRightMuted?.Invoke();
    }

    private void OnScrollListened(InputAction.CallbackContext context)
    {
        scrollListened?.Invoke(context.ReadValue<float>());
    }

    private void OnScrollMuted(InputAction.CallbackContext context)
    {
        scrollMuted?.Invoke(context.ReadValue<float>());
    }

    private void OnSpecialActionListened(InputAction.CallbackContext context)
    {
        specialAction?.Invoke(true);
    }

    private void OnSpecialActionMuted(InputAction.CallbackContext context)
    {
        specialAction?.Invoke(false);
    }
}
