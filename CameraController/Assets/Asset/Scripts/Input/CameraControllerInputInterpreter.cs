﻿using UnityEngine;

/// <summary>
/// Interprets hardware input and translates it into something that other controllers can understand 
/// </summary>
public class CameraControllerInputInterpreter : MonoBehaviour
{
    /// <summary>
    /// Fires when a movement in a direction occurs
    /// </summary>
    public event movementVectorDelegate movementSet;

    /// <summary>
    /// Fires when a movement in a direction stops
    /// </summary>
    public event movementVectorDelegate movementMuted;

    /// <summary>
    /// Fires when a rotation in a direction occurs  
    /// </summary>
    public event movementVectorDelegate rotationSet;

    /// <summary>
    /// Fires when a rotation in a direction stops
    /// </summary>
    public event movementVectorDelegate rotationMuted;

    /// <summary>
    /// Fires when mouse scroll occurs
    /// </summary>
    public event movementVectorDelegate scrollSet;

    /// <summary>
    /// Fires when a mouse scroll stops
    /// </summary>
    public event movementVectorDelegate scrollMuted;

    /// <summary>
    /// Fires to indicate, whether a special action is occuring
    /// </summary>
    public event valueDelegate<bool> specialAction;
  

    [SerializeField] private CameraControllerInputListener _InputListener = null;

    void Awake()
    {
        _InputListener.movementForwardListened += OnForwardMovementListened;
        _InputListener.movementBackwardListened += OnBackwardMovementListened;
        _InputListener.movementLeftListened += OnLeftMovementListened;
        _InputListener.movementRightListened += OnRightMovementListened;
        _InputListener.movementForwardMuted += OnForwardMovementMuted;
        _InputListener.movementBackwardMuted += OnBackwardMovementMuted;
        _InputListener.movementLeftMuted += OnLeftMovementMuted;
        _InputListener.movementRightMuted += OnRightMovementMuted;

        _InputListener.rotateLeftListened += OnLeftRotationListened;
        _InputListener.rotateRightListened += OnRightRotationListened;
        _InputListener.rotateLeftMuted += OnLeftRotationMuted;
        _InputListener.rotateRightMuted += OnRightRotationMuted;

        _InputListener.scrollListened += OnScrollListened;
        _InputListener.scrollMuted += OnScrollMuted;
        _InputListener.specialAction += OnSpecialAction;
    }

    private void OnForwardMovementListened()
    {
        movementSet?.Invoke(MovementVectors.FWD);
    }

    private void OnForwardMovementMuted()
    {
        movementMuted?.Invoke(MovementVectors.FWD);   
    }

    private void OnBackwardMovementListened()
    {
        movementSet?.Invoke(MovementVectors.BWD);
    }

    private void OnBackwardMovementMuted()
    {
        movementMuted?.Invoke(MovementVectors.BWD); 
    }

    private void OnLeftMovementListened()
    {
        movementSet?.Invoke(MovementVectors.LFT);
    }

    private void OnLeftMovementMuted()
    {
        movementMuted?.Invoke(MovementVectors.LFT); 
    }

    private void OnLeftRotationListened()
    {
        rotationSet?.Invoke(MovementVectors.LFT);
    }

    private void OnLeftRotationMuted()
    {
        rotationMuted?.Invoke(MovementVectors.LFT);
    }

    private void OnRightMovementListened()
    {
        movementSet?.Invoke(MovementVectors.RGHT);
    }

    private void OnRightMovementMuted()
    {
        movementMuted?.Invoke(MovementVectors.RGHT); 
    }

    private void OnRightRotationListened()
    {
        rotationSet?.Invoke(MovementVectors.RGHT);
    }

    private void OnRightRotationMuted()
    {
        rotationMuted?.Invoke(MovementVectors.RGHT);
    }


    private void OnScrollListened(float value)
    {
        if(value > 0)
        {
            scrollSet?.Invoke(MovementVectors.UP);
        }
        else if(value < 0)
        {
            scrollSet?.Invoke(MovementVectors.DOWN);
        }
    }

    private void OnScrollMuted(float value)
    {
        scrollMuted?.Invoke(MovementVectors.NONE);
    }

    private void OnSpecialAction(bool value)
    {
        specialAction?.Invoke(value);
    }
}
