// GENERATED AUTOMATICALLY FROM 'Assets/Asset/Scripts/Input/CameraControllerInput.inputactions'

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Utilities;

public class @CameraControllerInput : IInputActionCollection, IDisposable
{
    public InputActionAsset asset { get; }
    public @CameraControllerInput()
    {
        asset = InputActionAsset.FromJson(@"{
    ""name"": ""CameraControllerInput"",
    ""maps"": [
        {
            ""name"": ""StandardInput"",
            ""id"": ""3b287755-9434-4e66-ab8b-8ced789f8e42"",
            ""actions"": [
                {
                    ""name"": ""MovementForward"",
                    ""type"": ""Button"",
                    ""id"": ""eb2f7f9b-eb72-4244-9011-29123800c7ce"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""MovementBackward"",
                    ""type"": ""Button"",
                    ""id"": ""96ee8343-7a62-464c-8ba5-f5754ef77c9c"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""MovementLeft"",
                    ""type"": ""Button"",
                    ""id"": ""0107630e-10b4-4d4f-a6df-183b952d6a12"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""MovementRight"",
                    ""type"": ""Button"",
                    ""id"": ""87d17598-e7d6-4982-b5ed-142688295166"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""RotateLeft"",
                    ""type"": ""Button"",
                    ""id"": ""8662312d-cb46-4c80-9467-e36d3529c50b"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""RotateRight"",
                    ""type"": ""Button"",
                    ""id"": ""d7abb5ab-e67a-42a1-8253-7532caaf5675"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""SpecialAction"",
                    ""type"": ""Button"",
                    ""id"": ""02264ec4-55f5-4f34-93ca-b8e45d256019"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Scroll"",
                    ""type"": ""Value"",
                    ""id"": ""ee522302-e5c8-4fcc-ae7b-90652456dc6c"",
                    ""expectedControlType"": ""Axis"",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": """",
                    ""id"": ""505a015f-7590-4f3e-9748-d69557b9c716"",
                    ""path"": ""<Keyboard>/w"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""MovementForward"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""cac73006-8e3a-4605-a0c8-7233458efe97"",
                    ""path"": ""<Keyboard>/s"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""MovementBackward"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""eddd6238-e66c-48da-8dae-b48f272fdd24"",
                    ""path"": ""<Keyboard>/a"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""MovementLeft"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""a242d2cd-5b35-4924-a5e9-f14e7d755759"",
                    ""path"": ""<Keyboard>/d"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""MovementRight"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""ebf679ef-eda8-4645-b50d-b559e83665e5"",
                    ""path"": ""<Keyboard>/q"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""RotateLeft"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""f9b3f496-564f-47c6-bcb4-abe214f579e7"",
                    ""path"": ""<Keyboard>/e"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""RotateRight"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""723e9636-df3c-4cf4-8360-0638b48712a9"",
                    ""path"": ""<Keyboard>/leftCtrl"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""SpecialAction"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""41e35315-f145-4e8a-b86e-255a23eee3f2"",
                    ""path"": ""<Mouse>/scroll/y"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Scroll"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        }
    ],
    ""controlSchemes"": []
}");
        // StandardInput
        m_StandardInput = asset.FindActionMap("StandardInput", throwIfNotFound: true);
        m_StandardInput_MovementForward = m_StandardInput.FindAction("MovementForward", throwIfNotFound: true);
        m_StandardInput_MovementBackward = m_StandardInput.FindAction("MovementBackward", throwIfNotFound: true);
        m_StandardInput_MovementLeft = m_StandardInput.FindAction("MovementLeft", throwIfNotFound: true);
        m_StandardInput_MovementRight = m_StandardInput.FindAction("MovementRight", throwIfNotFound: true);
        m_StandardInput_RotateLeft = m_StandardInput.FindAction("RotateLeft", throwIfNotFound: true);
        m_StandardInput_RotateRight = m_StandardInput.FindAction("RotateRight", throwIfNotFound: true);
        m_StandardInput_SpecialAction = m_StandardInput.FindAction("SpecialAction", throwIfNotFound: true);
        m_StandardInput_Scroll = m_StandardInput.FindAction("Scroll", throwIfNotFound: true);
    }

    public void Dispose()
    {
        UnityEngine.Object.Destroy(asset);
    }

    public InputBinding? bindingMask
    {
        get => asset.bindingMask;
        set => asset.bindingMask = value;
    }

    public ReadOnlyArray<InputDevice>? devices
    {
        get => asset.devices;
        set => asset.devices = value;
    }

    public ReadOnlyArray<InputControlScheme> controlSchemes => asset.controlSchemes;

    public bool Contains(InputAction action)
    {
        return asset.Contains(action);
    }

    public IEnumerator<InputAction> GetEnumerator()
    {
        return asset.GetEnumerator();
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return GetEnumerator();
    }

    public void Enable()
    {
        asset.Enable();
    }

    public void Disable()
    {
        asset.Disable();
    }

    // StandardInput
    private readonly InputActionMap m_StandardInput;
    private IStandardInputActions m_StandardInputActionsCallbackInterface;
    private readonly InputAction m_StandardInput_MovementForward;
    private readonly InputAction m_StandardInput_MovementBackward;
    private readonly InputAction m_StandardInput_MovementLeft;
    private readonly InputAction m_StandardInput_MovementRight;
    private readonly InputAction m_StandardInput_RotateLeft;
    private readonly InputAction m_StandardInput_RotateRight;
    private readonly InputAction m_StandardInput_SpecialAction;
    private readonly InputAction m_StandardInput_Scroll;
    public struct StandardInputActions
    {
        private @CameraControllerInput m_Wrapper;
        public StandardInputActions(@CameraControllerInput wrapper) { m_Wrapper = wrapper; }
        public InputAction @MovementForward => m_Wrapper.m_StandardInput_MovementForward;
        public InputAction @MovementBackward => m_Wrapper.m_StandardInput_MovementBackward;
        public InputAction @MovementLeft => m_Wrapper.m_StandardInput_MovementLeft;
        public InputAction @MovementRight => m_Wrapper.m_StandardInput_MovementRight;
        public InputAction @RotateLeft => m_Wrapper.m_StandardInput_RotateLeft;
        public InputAction @RotateRight => m_Wrapper.m_StandardInput_RotateRight;
        public InputAction @SpecialAction => m_Wrapper.m_StandardInput_SpecialAction;
        public InputAction @Scroll => m_Wrapper.m_StandardInput_Scroll;
        public InputActionMap Get() { return m_Wrapper.m_StandardInput; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(StandardInputActions set) { return set.Get(); }
        public void SetCallbacks(IStandardInputActions instance)
        {
            if (m_Wrapper.m_StandardInputActionsCallbackInterface != null)
            {
                @MovementForward.started -= m_Wrapper.m_StandardInputActionsCallbackInterface.OnMovementForward;
                @MovementForward.performed -= m_Wrapper.m_StandardInputActionsCallbackInterface.OnMovementForward;
                @MovementForward.canceled -= m_Wrapper.m_StandardInputActionsCallbackInterface.OnMovementForward;
                @MovementBackward.started -= m_Wrapper.m_StandardInputActionsCallbackInterface.OnMovementBackward;
                @MovementBackward.performed -= m_Wrapper.m_StandardInputActionsCallbackInterface.OnMovementBackward;
                @MovementBackward.canceled -= m_Wrapper.m_StandardInputActionsCallbackInterface.OnMovementBackward;
                @MovementLeft.started -= m_Wrapper.m_StandardInputActionsCallbackInterface.OnMovementLeft;
                @MovementLeft.performed -= m_Wrapper.m_StandardInputActionsCallbackInterface.OnMovementLeft;
                @MovementLeft.canceled -= m_Wrapper.m_StandardInputActionsCallbackInterface.OnMovementLeft;
                @MovementRight.started -= m_Wrapper.m_StandardInputActionsCallbackInterface.OnMovementRight;
                @MovementRight.performed -= m_Wrapper.m_StandardInputActionsCallbackInterface.OnMovementRight;
                @MovementRight.canceled -= m_Wrapper.m_StandardInputActionsCallbackInterface.OnMovementRight;
                @RotateLeft.started -= m_Wrapper.m_StandardInputActionsCallbackInterface.OnRotateLeft;
                @RotateLeft.performed -= m_Wrapper.m_StandardInputActionsCallbackInterface.OnRotateLeft;
                @RotateLeft.canceled -= m_Wrapper.m_StandardInputActionsCallbackInterface.OnRotateLeft;
                @RotateRight.started -= m_Wrapper.m_StandardInputActionsCallbackInterface.OnRotateRight;
                @RotateRight.performed -= m_Wrapper.m_StandardInputActionsCallbackInterface.OnRotateRight;
                @RotateRight.canceled -= m_Wrapper.m_StandardInputActionsCallbackInterface.OnRotateRight;
                @SpecialAction.started -= m_Wrapper.m_StandardInputActionsCallbackInterface.OnSpecialAction;
                @SpecialAction.performed -= m_Wrapper.m_StandardInputActionsCallbackInterface.OnSpecialAction;
                @SpecialAction.canceled -= m_Wrapper.m_StandardInputActionsCallbackInterface.OnSpecialAction;
                @Scroll.started -= m_Wrapper.m_StandardInputActionsCallbackInterface.OnScroll;
                @Scroll.performed -= m_Wrapper.m_StandardInputActionsCallbackInterface.OnScroll;
                @Scroll.canceled -= m_Wrapper.m_StandardInputActionsCallbackInterface.OnScroll;
            }
            m_Wrapper.m_StandardInputActionsCallbackInterface = instance;
            if (instance != null)
            {
                @MovementForward.started += instance.OnMovementForward;
                @MovementForward.performed += instance.OnMovementForward;
                @MovementForward.canceled += instance.OnMovementForward;
                @MovementBackward.started += instance.OnMovementBackward;
                @MovementBackward.performed += instance.OnMovementBackward;
                @MovementBackward.canceled += instance.OnMovementBackward;
                @MovementLeft.started += instance.OnMovementLeft;
                @MovementLeft.performed += instance.OnMovementLeft;
                @MovementLeft.canceled += instance.OnMovementLeft;
                @MovementRight.started += instance.OnMovementRight;
                @MovementRight.performed += instance.OnMovementRight;
                @MovementRight.canceled += instance.OnMovementRight;
                @RotateLeft.started += instance.OnRotateLeft;
                @RotateLeft.performed += instance.OnRotateLeft;
                @RotateLeft.canceled += instance.OnRotateLeft;
                @RotateRight.started += instance.OnRotateRight;
                @RotateRight.performed += instance.OnRotateRight;
                @RotateRight.canceled += instance.OnRotateRight;
                @SpecialAction.started += instance.OnSpecialAction;
                @SpecialAction.performed += instance.OnSpecialAction;
                @SpecialAction.canceled += instance.OnSpecialAction;
                @Scroll.started += instance.OnScroll;
                @Scroll.performed += instance.OnScroll;
                @Scroll.canceled += instance.OnScroll;
            }
        }
    }
    public StandardInputActions @StandardInput => new StandardInputActions(this);
    public interface IStandardInputActions
    {
        void OnMovementForward(InputAction.CallbackContext context);
        void OnMovementBackward(InputAction.CallbackContext context);
        void OnMovementLeft(InputAction.CallbackContext context);
        void OnMovementRight(InputAction.CallbackContext context);
        void OnRotateLeft(InputAction.CallbackContext context);
        void OnRotateRight(InputAction.CallbackContext context);
        void OnSpecialAction(InputAction.CallbackContext context);
        void OnScroll(InputAction.CallbackContext context);
    }
}
