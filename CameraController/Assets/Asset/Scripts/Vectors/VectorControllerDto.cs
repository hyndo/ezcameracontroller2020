﻿using System;

/// <summary>
/// Data transfer object to initialize VectorController
/// </summary>
public struct VectorControllerDto 
{
    // ACTION TYPE VALS
    public Array cameraActionTypeVals {get; set; }
}
