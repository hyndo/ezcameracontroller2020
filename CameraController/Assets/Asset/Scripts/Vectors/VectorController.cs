﻿using System.Collections.Generic;

/// <summary>
/// Holds vectors for the camera actions 
/// </summary>
public class VectorController 
{
    /// <summary>
    /// Main constructor
    /// </summary>
    /// <param name="dto"></param>
    public VectorController(VectorControllerDto dto)
    {
        ActiveVectors = new Dictionary<CameraActionTypes, List<MovementVectors>>();
        LastActiveVector = new Dictionary<CameraActionTypes, MovementVectors>();
        for(int i=0; i< dto.cameraActionTypeVals.Length; i++)
        {
            CameraActionTypes type = (CameraActionTypes)i;
            ActiveVectors.Add(type, new List<MovementVectors>());
            LastActiveVector.Add(type, MovementVectors.NONE);
        }
    }

    /// <summary>
    /// Vectors used by camera actions
    /// </summary>
    public Dictionary<CameraActionTypes, List<MovementVectors>>                ActiveVectors {get; set;}

    /// <summary>
    /// Last vectors useb by camera actions
    /// </summary>
    public Dictionary<CameraActionTypes, MovementVectors>                      LastActiveVector {get; set;}
}
