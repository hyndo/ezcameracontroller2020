﻿using UnityEngine;

/// <summary>
/// Contains constraints for the camera controllers
/// </summary>
public struct Constraints 
{
    /// <summary>
    /// Main constructor
    /// </summary>
    /// <param name="fovConstraints"></param>
    /// <param name="verticalConstraints"></param>
    /// <param name="horizontalConstraints"></param>
    /// <param name="xAxisRotationConstraint"></param>
    public Constraints(Vector2 fovConstraints, 
    Vector2 verticalConstraints, 
    Vector4 horizontalConstraints,
    Vector2 xAxisRotationConstraint)
    {
        FOVConstraint = fovConstraints;
        VerticalConstraint = verticalConstraints;
        HorizontalConstraint = horizontalConstraints;
        XAxisRotationConstraint = xAxisRotationConstraint;
    }

    /// <summary>
    /// Constraint for the range of fov that is allowed for the camera
    /// </summary>
    public Vector2 FOVConstraint{get; private set;}

    /// <summary>
    /// Constraint for the range of angles, that are allowed in the camera x axis
    /// </summary>
    public Vector2 XAxisRotationConstraint{get; private set;}

    /// <summary>
    /// Constraint for the height above terrain aallowed for the camera 
    /// </summary>
    public Vector2 VerticalConstraint{get; private set;}

    /// <summary>
    /// Constraint for the horizontal plane of camera movement
    /// </summary>
    public Vector4 HorizontalConstraint {get; private set;}

    
    /// <summary>
    /// Checks if all given constraints are in the right format
    /// </summary>
    /// <returns></returns>
    public bool CheckValidity()
    {
        bool checkVerticalConstraints = VerticalConstraint.x < VerticalConstraint.y;
        bool checkHorizontalConstraints = HorizontalConstraint.x < HorizontalConstraint.y && HorizontalConstraint.z < HorizontalConstraint.w;
        return checkVerticalConstraints && checkHorizontalConstraints;
    }
}
