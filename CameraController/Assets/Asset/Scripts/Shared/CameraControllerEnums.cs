﻿/// <summary>
/// Directions, in which camera actions can be taken
/// </summary>
public enum MovementVectors { NONE, FWD, BWD, LFT, RGHT, UP, DOWN };

/// <summary>
/// Possible actions for the camera
/// </summary>
public enum CameraActionTypes {MOVEMENT=0, ROTATION=1, SCROLL=2, ZOOM=3}

