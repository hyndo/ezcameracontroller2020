﻿/// <summary>
/// Contains all constants for EZCameraController
/// </summary>
public sealed class CameraControllerConstants 
{
    public const float HALF_ARC = 180;
    public const float QUARTER_ARC = 90;

    public const float HORIZONTAL_CONSTRAINTS_LIMIT_MULTIPLIER = 5f;
    public const float FOV_ANGLE_POSITIVE_MULTIPLIER = 0.8f;
    public const float FOV_ANGLE_NEGATIVE_MULTIPLIER = 0.5f;

    public const float MINIMAL_X_AXIS_ROTATION_NEGATIVE_RAW = -45;
    public const float MINIMAL_X_AXIS_ROTATION_NEGATIVE = 315;
    public const float MAXIMAL_X_AXIS_ROTATION_NEGATIVE = 360;
    public const float MINIMAL_X_AXIS_ROTATION = 0;
    public const float NORMAL_X_AXIS_ROTATION = 45;
    public const float MAXIMAL_X_AXIS_ROTATION = 90;
}
