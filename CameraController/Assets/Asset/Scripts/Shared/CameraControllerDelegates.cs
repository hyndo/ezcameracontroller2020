﻿/// <summary>
/// Delegate that takes a vector as an argument
/// </summary>
/// <param name="vector"></param>
public delegate void movementVectorDelegate(MovementVectors vector);

/// <summary>
/// Delegate that takes an argument of generic type
/// </summary>
/// <typeparam name="T"></typeparam>
/// <param name="value"></param>
public delegate void valueDelegate<T> (T value);

/// <summary>
/// Delegate that takes a single argument and returns a single value of generic type
/// </summary>
/// <typeparam name="T"></typeparam>
/// <param name="value"></param>
/// <returns></returns>
public delegate T returnDelegate<T>(T value);

/// <summary>
/// Delegate that takes no arguments and returns nothing
/// </summary>
public delegate void muteDelegate();